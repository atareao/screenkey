#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import dbus

bus = dbus.SessionBus()

if __name__ == '__main__':
    try:
        screenkey_service = bus.get_object('com.screenkey.Screenkey',
                                           '/com/screenkey/Screenkey')
        change_state = screenkey_service.get_dbus_method(
            'change_state', 'com.screenkey.Screenkey')
        change_state()
    except dbus.exceptions.DBusException as an_exception:
        print(an_exception)
    exit(0)
