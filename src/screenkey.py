#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# "screenkey" is distributed under GNU GPLv3+, WITHOUT ANY WARRANTY.
# Copyright(c) 2010-2012: Pablo Seminario <pabluk@gmail.com>
# Copyright(c) 2015-2016: wave++ "Yuri D'Elia" <wavexx@thregr.org>
# Copyright(c) 2019-2020: Yuto Tokunaga <yuntan.sub1@gmail.com>

from labelmanager import LabelManager

from threading import Timer
import json
import os
import gi
try:
    gi.require_version('Gtk', '3.0')
    gi.require_version('Gdk', '3.0')
    gi.require_version('Pango', '1.0')
    gi.require_version('AppIndicator3', '0.1')
    gi.require_version('GLib', '2.0')
except Exception as e:
    print(e)
    exit(-1)
from gi.repository import Gtk
from gi.repository import Gdk
from gi.repository import Pango
from gi.repository import AppIndicator3
from gi.repository import GLib
from config import Options, APP_NAME, VERSION, APP_DESC, APP_URL, ERROR_URL, \
                   STATE_FILE, DEFAULTS, ICON_ACTIVED_LIGHT, ICON_PAUSED_LIGHT
from preferences_dialog import PreferencesDialog
from config import _
import cairo

GLib.threads_init()

START = Gtk.Align.START
CENTER = Gtk.Align.CENTER
END = Gtk.Align.END
FILL = Gtk.Align.FILL
TOP = Gtk.PositionType.TOP
BOTTOM = Gtk.PositionType.BOTTOM
RIGHT = Gtk.PositionType.RIGHT
LEFT = Gtk.PositionType.LEFT
HORIZONTAL = Gtk.Orientation.HORIZONTAL
VERTICAL = Gtk.Orientation.VERTICAL
IF_VALID = Gtk.SpinButtonUpdatePolicy.IF_VALID


class Screenkey(Gtk.Window):

    def __init__(self, logger):
        Gtk.Window.__init__(self, Gtk.WindowType.POPUP)
        self.exit_status = None
        self.timer_hide = None
        self.timer_min = None
        self.logger = logger

        self.set_keep_above(True)
        self.set_accept_focus(False)
        self.set_focus_on_map(False)
        self.set_app_paintable(True)

        self.label = Gtk.Label()
        self.label.set_attributes(Pango.AttrList())
        self.label.set_ellipsize(Pango.EllipsizeMode.START)
        self.label.set_justify(Gtk.Justification.CENTER)
        self.label.show()
        self.add(self.label)

        self.load_state()
        self.font = Pango.FontDescription(self.options.font_desc)
        self.update_colors()

        self.set_size_request(0, 0)
        self.set_gravity(Gdk.Gravity.CENTER)
        self.connect("configure-event", self.on_configure)
        self.connect("draw", self.on_draw)

        scr = self.get_screen()
        scr.connect("size-changed", self.on_configure)
        scr.connect("monitors-changed", self.on_monitors_changed)
        self.set_active_monitor(self.options.screen)

        visual = scr.get_rgba_visual()
        if visual is not None:
            self.set_visual(visual)

        self.labelmngr = None
        self.enabled = True
        self.on_change_mode()

        self.make_menu()
        self.make_about_dialog()

        if not self.options.no_systray:
            self.make_systray()

        self.connect("delete-event", self.quit)
        if self.options.persist:
            self.show()

    def quit(self, widget=None, data=None, exit_status=os.EX_OK):
        self.labelmngr.stop()
        self.exit_status = exit_status
        Gtk.main_quit()

    def load_state(self):
        """Load stored options"""
        options = None
        try:
            with open(STATE_FILE, 'r') as f:
                options = Options(json.load(f))
                self.logger.debug("Options loaded.")
        except IOError:
            self.logger.debug("file %s does not exists." % STATE_FILE)
        except ValueError:
            self.logger.debug("file %s is invalid." % STATE_FILE)

        # compatibility with previous versions (0.5)
        if options and options.key_mode == 'normal':
            options.key_mode = 'composed'

        if options is None:
            options = DEFAULTS
        else:
            # copy missing defaults
            for k, v in DEFAULTS.items():
                if k not in options:
                    options[k] = v
        self.options = options

    def set_active_monitor(self, monitor):
        display = Gdk.Display.get_default()
        if monitor >= display.get_n_monitors():
            self.monitor = 0
        else:
            self.monitor = monitor
        self.update_geometry()

    def on_monitors_changed(self, *_):
        self.set_active_monitor(self.monitor)

    def update_font(self):
        _, window_height = self.get_size()
        text = self.label.get_text()
        lines = text.count('\n') + 1
        self.font.set_absolute_size(
                (50 * window_height // lines // 100) * 1000)
        self.label.get_pango_context().set_font_description(self.font)

    def update_colors(self):
        self.label.modify_fg(Gtk.StateFlags.NORMAL,
                             Gdk.color_parse(self.options.font_color))
        self.bg_color = Gdk.color_parse(self.options.bg_color)
        self.queue_draw()

    def on_draw(self, widget, cr):
        cr.set_source_rgba(self.bg_color.red_float,
                           self.bg_color.green_float,
                           self.bg_color.blue_float,
                           self.options.opacity)
        cr.set_operator(cairo.OPERATOR_SOURCE)
        cr.paint()
        return False

    def on_configure(self, *_):
        window_x, window_y = self.get_position()
        window_width, window_height = self.get_size()

        # TODO: set event mask
        # mask = Gdk.Pixmap(None, window_width, window_height, 1)
        # gc = Gdk.GC(mask)
        # gc.set_foreground(Gdk.Color(pixel=0))
        # mask.draw_rectangle(gc, True, 0, 0, window_width, window_height)
        # self.input_shape_combine_mask(mask, 0, 0)

        # set some proportional inner padding
        self.label.set_padding(window_width // 100, 0)

        self.update_font()

    def update_geometry(self, configure=False):
        display = Gdk.Display.get_default()
        if self.options.screen > display.get_n_monitors() - 1:
            self.options.screen = 0
        geometry = display.get_monitor(self.options.screen).get_geometry()
        if self.options.geometry is not None:

            g = self.options.geometry
            x = g[0]
            y = g[1]
            w = g[2]
            h = g[3]
            if len(g) == 6:
                inverse_offset_x = g[4]
                inverse_offset_y = g[5]
                if inverse_offset_x:
                    x = geometry.width - x - w
                if inverse_offset_y:
                    y = geometry.height - y - h
            area_geometry = [x, y, w, h]
        else:
            area_geometry = [geometry.x, geometry.y, geometry.width,
                             geometry.height]

        if self.options.position == 'fixed':
            self.move(*area_geometry[0:2])
            self.resize(*area_geometry[2:4])
            self.update_font()
            return

        if self.options.font_size == 'large':
            window_height = 24 * area_geometry[3] // 100
        elif self.options.font_size == 'medium':
            window_height = 12 * area_geometry[3] // 100
        else:
            window_height = 8 * area_geometry[3] // 100
        self.resize(area_geometry[2], window_height)

        if self.options.position == 'top':
            window_y = area_geometry[1] + area_geometry[3] // 10
        elif self.options.position == 'center':
            window_y = (area_geometry[1] + area_geometry[3] // 2 -
                        window_height // 2)
        else:
            window_y = (area_geometry[1] + area_geometry[3] * 9 // 10 -
                        window_height)
        self.move(area_geometry[0], window_y)
        self.update_font()

    def on_statusicon_popup(self, widget, button, timestamp, data=None):
        if button == 3 and data:
            data.show()
            data.popup_at_pointer(None)

    def show(self):
        super(Screenkey, self).show()

    def on_labelmngr_error(self):
        msg = Gtk.MessageDialog(parent=self,
                                type=Gtk.MessageType.ERROR,
                                buttons=Gtk.ButtonsType.OK,
                                message_format="Error initializing Screenkey")
        text = _('Screenkey failed to initialize. This is usually a sign of \
an improperly configured input method or desktop keyboard settings. Please \
see the <a href="{url}">troubleshooting documentation</a> for further \
diagnosing instructions.\n\nScreenkey cannot recover and will now quit!')
        msg.format_secondary_markup(text.format(url=ERROR_URL))
        msg.run()
        msg.destroy()
        self.quit(exit_status=os.EX_SOFTWARE)

    def on_label_change(self, markup, synthetic):
        if markup is None:
            self.on_labelmngr_error()
            return

        _, attr, text, _ = Pango.parse_markup(markup, -1, ' ')
        self.label.set_text(text)
        self.label.set_attributes(attr)
        self.update_font()

        if not self.get_property('visible'):
            self.show()
        if self.timer_hide:
            self.timer_hide.cancel()
        if self.options.timeout > 0:
            self.timer_hide = Timer(self.options.timeout, self.on_timeout_main)
            self.timer_hide.start()
        if self.timer_min:
            self.timer_min.cancel()
        if not synthetic:
            self.timer_min = Timer(self.options.recent_thr * 2,
                                   self.on_timeout_min)
            self.timer_min.start()

    def on_timeout_main(self):
        if not self.options.persist:
            self.hide()
        self.label.set_text('')
        self.labelmngr.clear()

    def on_timeout_min(self):
        self.labelmngr.queue_update()

    def restart_labelmanager(self):
        self.logger.debug("Restarting LabelManager.")
        if self.labelmngr:
            self.labelmngr.stop()
        self.labelmngr = LabelManager(self.on_label_change, logger=self.logger,
                                      key_mode=self.options.key_mode,
                                      bak_mode=self.options.bak_mode,
                                      mods_mode=self.options.mods_mode,
                                      mods_only=self.options.mods_only,
                                      multiline=self.options.multiline,
                                      vis_shift=self.options.vis_shift,
                                      vis_space=self.options.vis_space,
                                      recent_thr=self.options.recent_thr,
                                      compr_cnt=self.options.compr_cnt,
                                      ignore=self.options.ignore,
                                      pango_ctx=self.label.get_pango_context())
        self.labelmngr.start()

    def on_change_mode(self):
        if not self.enabled:
            return
        self.restart_labelmanager()

    def change_state(self):
        self.enabled = not self.enabled
        self.toggle(None)

    def toggle(self, widget, data=None):
        print(widget)
        if self.enabled:
            self.logger.debug("Screenkey enabled.")
            self.restart_labelmanager()
            GLib.idle_add(self.systray.set_icon, ICON_ACTIVED_LIGHT)
        else:
            self.logger.debug("Screenkey disabled.")
            self.labelmngr.stop()
            GLib.idle_add(self.systray.set_icon, ICON_PAUSED_LIGHT)

    def on_show_keys(self, widget, data=None):
        self.enabled = widget.get_active()
        self.toggle(widget)

    def on_preferences_dialog(self, widget=None, data=None):
        preferencesDialog = PreferencesDialog(self.logger)
        if preferencesDialog.run() == Gtk.ResponseType.ACCEPT:
            self.load_state()
            self.update_colors()
            self.update_geometry()
            self.update_font()
            self.restart_labelmanager()
        preferencesDialog.destroy()

    def make_menu(self):
        self.menu = menu = Gtk.Menu()

        show_item = Gtk.CheckMenuItem(_("Show keys"))
        show_item.set_active(True)
        show_item.connect("toggled", self.on_show_keys)
        show_item.show()
        menu.append(show_item)

        preferences_item = Gtk.MenuItem(_("Preferences"))
        preferences_item.connect("activate", self.on_preferences_dialog)
        preferences_item.show()
        menu.append(preferences_item)

        about_item = Gtk.MenuItem(_("About"))
        about_item.connect("activate", self.on_about_dialog)
        about_item.show()
        menu.append(about_item)

        separator_item = Gtk.SeparatorMenuItem()
        separator_item.show()
        menu.append(separator_item)

        image = Gtk.MenuItem(_("Quit"))
        image.connect("activate", self.quit)
        image.show()
        menu.append(image)
        menu.show()

    def make_systray(self):
        try:
            self.systray = AppIndicator3.Indicator.new(
                    'ScreenKey',
                    'ScreenKey',
                    AppIndicator3.IndicatorCategory.APPLICATION_STATUS)
            self.systray.set_label('', '')
            self.systray.set_status(AppIndicator3.IndicatorStatus.ACTIVE)
            self.systray.set_attention_icon("indicator-messages-new")
            self.systray.set_icon("screenkey")
            self.systray.set_menu(self.menu)
            self.logger.debug("Using AppIndicator.")
        except ImportError:
            self.systray = Gtk.StatusIcon()
            self.systray.set_from_icon_name(
                "preferences-desktop-keyboard-shortcuts")
            self.systray.connect("popup-menu", self.on_statusicon_popup,
                                 self.menu)
            self.logger.debug("Using StatusIcon.")

    def make_about_dialog(self):
        self.about = about = Gtk.AboutDialog(use_header_bar=True)
        about.set_program_name(APP_NAME)
        about.set_version(VERSION)
        about.set_copyright("""
        Copyright(c) 2010-2012: Pablo Seminario <pabluk@gmail.com>
        Copyright(c) 2015-2016: wave++ "Yuri D'Elia" <wavexx@thregr.org>
        """)
        about.set_comments(APP_DESC)
        about.set_documenters(
                ["José María Quiroga <pepelandia@gmail.com>"]
        )
        about.set_website(APP_URL)
        about.set_icon_name('screenkey')
        about.set_logo_icon_name('preferences-desktop-keyboard-shortcuts')
        about.connect("response", lambda *_: about.hide_on_delete())
        about.connect("delete-event", lambda *_: about.hide_on_delete())

    def on_about_dialog(self, widget, data=None):
        self.about.show()

    def run(self):
        Gtk.main()
        return self.exit_status
