#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import gi
try:
    gi.require_version('GLib', '2.0')
except Exception as exception:
    print(exception)
    exit(-1)
from gi.repository import GLib
try:
    from collections.abc import MutableMapping
except ImportError:
    # for python<3.3
    from collections import MutableMapping
import os
import locale
import gettext

APP = 'screenkey'
LANGDIR = os.path.join('usr', 'share', 'locale-langpack')

try:
    current_locale, encoding = locale.getdefaultlocale()
    language = gettext.translation(APP, LANGDIR, [current_locale])
    language.install()
    _ = language.ugettext
except Exception as e:
    print(e)
    _ = str

# Screenkey version
CONFIG_DIR = os.path.join(GLib.get_user_config_dir(), APP)
if not os.path.exists(CONFIG_DIR):
    os.makedirs(CONFIG_DIR, 0o700)
STATE_FILE = os.path.join(CONFIG_DIR, '{}.json'.format(APP))
APP_NAME = _("Screenkey")
APP_DESC = _("Screencast your keys")
APP_URL = 'https://www.thregr.org/~wavexx/software/screenkey/'
VERSION = '0.10'
ICON = 'screenkey'

if __file__.startswith('/usr') or os.getcwd().startswith('/usr'):
    ICONDIR = '/usr/share/icons'
else:
    ROOTDIR = os.path.abspath(os.path.dirname(__file__))
    ICONDIR = os.path.normpath(os.path.join(ROOTDIR, '../data/icons/'))

ICON_ACTIVED_LIGHT = os.path.join(ICONDIR, 'screenkey-active-light.svg')
ICON_PAUSED_LIGHT = os.path.join(ICONDIR, 'screenkey-paused-light.svg')
ICON_ACTIVED_DARK = os.path.join(ICONDIR, 'screenkey-active-dark.svg')
ICON_PAUSED_DARK = os.path.join(ICONDIR, 'screenkey-paused-dark.svg')

SLOP_URL = 'https://github.com/naelstrof/slop'
ERROR_URL = 'https://www.thregr.org/~wavexx/software/screenkey/'

# CLI/Interface options
POSITIONS = {
    'top': _('Top'),
    'center': _('Center'),
    'bottom': _('Bottom'),
    'fixed': _('Fixed'),
}

FONT_SIZES = {
    'large': _('Large'),
    'medium': _('Medium'),
    'small': _('Small'),
}

KEY_MODES = {
    'composed': _('Composed'),
    'translated': _('Translated'),
    'keysyms': _('Keysyms'),
    'raw': _('Raw'),
}

BAK_MODES = {
    'normal': _('Normal'),
    'baked': _('Baked'),
    'full': _('Full'),
}

MODS_MODES = {
    'normal': _('Normal'),
    'emacs': _('Emacs'),
    'mac': _('Mac'),
    'win': _('Windows'),
    'tux': _('Linux'),
}


class Options(MutableMapping):
    def __init__(self, *args, **kw):
        self.__dict__['_store'] = dict(*args, **kw)

    def __getitem__(self, key):
        return self._store[key]

    def __setitem__(self, key, value):
        self._store[key] = value

    def __delitem__(self, key):
        del self._store[key]

    def __iter__(self):
        return iter(self._store)

    def __len__(self):
        return len(self._store)

    def __getattr__(self, key):
        return self._store[key]

    def __setattr__(self, key, value):
        self._store[key] = value

    def __delattr__(self, key):
        del self._store[key]


DEFAULTS = Options({'no_systray': False,
                    'timeout': 2.5,
                    'recent_thr': 0.1,
                    'compr_cnt': 3,
                    'ignore': [],
                    'position': 'bottom',
                    'persist': False,
                    'font_desc': 'Sans Bold',
                    'font_size': 'medium',
                    'font_color': 'white',
                    'bg_color': 'black',
                    'opacity': 0.8,
                    'key_mode': 'composed',
                    'bak_mode': 'baked',
                    'mods_mode': 'normal',
                    'mods_only': False,
                    'multiline': False,
                    'vis_shift': False,
                    'vis_space': True,
                    'geometry': None,
                    'screen': 0,
                    'keybinding': '<Ctrl><Super><s>'})



def get_desktop_environment():
    desktop_session = os.environ.get("DESKTOP_SESSION")
    # easier to match if we doesn't have  to deal with caracter cases
    if desktop_session is not None:
        desktop_session = desktop_session.lower()
        if desktop_session in ["gnome", "unity", "cinnamon", "mate",
                               "budgie-desktop", "xfce4", "lxde", "fluxbox",
                               "blackbox", "openbox", "icewm", "jwm",
                               "afterstep", "trinity", "kde"]:
            return desktop_session
        # ## Special cases ##
        # Canonical sets $DESKTOP_SESSION to Lubuntu rather than
        # LXDE if using LXDE.
        # There is no guarantee that they will not do the same with
        # the other desktop environments.
        elif "xfce" in desktop_session or\
                desktop_session.startswith("xubuntu"):
            return "xfce4"
        elif desktop_session.startswith("ubuntu"):
            return "unity"
        elif desktop_session.startswith("lubuntu"):
            return "lxde"
        elif desktop_session.startswith("kubuntu"):
            return "kde"
        elif desktop_session.startswith("razor"):  # e.g. razorkwin
            return "razor-qt"
        elif desktop_session.startswith("wmaker"):  # eg. wmaker-common
            return "windowmaker"
    if os.environ.get('KDE_FULL_SESSION') == 'true':
        return "kde"
    elif os.environ.get('GNOME_DESKTOP_SESSION_ID'):
        if "deprecated" not in os.environ.get(
                'GNOME_DESKTOP_SESSION_ID'):
            return "gnome2"
    # From http://ubuntuforums.org/showthread.php?t=652320
    elif is_running("xfce-mcs-manage"):
        return "xfce4"
    elif is_running("ksmserver"):
        return "kde"
    return "unknown"
