#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# "screenkey" is distributed under GNU GPLv3+, WITHOUT ANY WARRANTY.
# Copyright(c) 2010-2012: Pablo Seminario <pabluk@gmail.com>
# Copyright(c) 2015-2016: wave++ "Yuri D'Elia" <wavexx@thregr.org>
# Copyright(c) 2019-2020: Yuto Tokunaga <yuntan.sub1@gmail.com>

import gi
try:
    gi.require_version('Gtk', '3.0')
    gi.require_version('Gdk', '3.0')
except Exception as e:
    print(e)
    exit(-1)
from gi.repository import Gtk
from gi.repository import Gdk
from basedialog import BaseDialog
from dconfigurator import DConfManager
from config import SLOP_URL, POSITIONS, FONT_SIZES, KEY_MODES, \
    BAK_MODES, MODS_MODES, STATE_FILE, DEFAULTS
from wait_keybind import WaitKeybind
import subprocess
import json
from config import _, Options, get_desktop_environment

START = Gtk.Align.START
CENTER = Gtk.Align.CENTER
END = Gtk.Align.END
FILL = Gtk.Align.FILL
TOP = Gtk.PositionType.TOP
BOTTOM = Gtk.PositionType.BOTTOM
RIGHT = Gtk.PositionType.RIGHT
LEFT = Gtk.PositionType.LEFT
HORIZONTAL = Gtk.Orientation.HORIZONTAL
VERTICAL = Gtk.Orientation.VERTICAL
IF_VALID = Gtk.SpinButtonUpdatePolicy.IF_VALID


class PreferencesDialog(BaseDialog):

    def __init__(self, logger):
        self.logger = logger
        BaseDialog.__init__(self, _('Preferences'), None, ok_button=True,
                            cancel_button=True)
        self.connect("response", self.on_preferences_changed)

    def on_preferences_changed(self, widget=None, data=None):
        if data == Gtk.ResponseType.ACCEPT:
            self.store_state(self.options)

    def load_state(self):
        """Load stored options"""
        options = None
        try:
            with open(STATE_FILE, 'r') as f:
                options = Options(json.load(f))
                self.logger.debug("Options loaded.")
        except IOError:
            self.logger.debug("file %s does not exists." % STATE_FILE)
        except ValueError:
            self.logger.debug("file %s is invalid." % STATE_FILE)

        # compatibility with previous versions (0.5)
        if options and options.key_mode == 'normal':
            options.key_mode = 'composed'

        if options is None:
            options = DEFAULTS
        else:
            # copy missing defaults
            for k, v in DEFAULTS.items():
                if k not in options:
                    options[k] = v
        return options

    def store_state(self, options):
        """Store options"""
        try:
            with open(STATE_FILE, 'w') as f:
                json.dump(options._store, f)
                self.logger.debug("Options saved.")
        except IOError:
            self.logger.debug("Cannot open %s." % STATE_FILE)

    def on_sb_time_changed(self, widget, data=None):
        self.options.timeout = widget.get_value()
        self.logger.debug("Timeout value changed: %f." % self.options.timeout)

    def on_cbox_sizes_changed(self, widget, data=None):
        self.options.font_size = widget.props.active_id
        self.logger.debug("Window size changed: %s." % self.options.font_size)

    def on_cbox_modes_changed(self, widget, data=None):
        self.options.key_mode = widget.props.active_id
        self.logger.debug("Key mode changed: %s." % self.options.key_mode)

    def on_cbox_bak_changed(self, widget, data=None):
        self.options.bak_mode = widget.props.active_id
        self.logger.debug("Bak mode changed: %s." % self.options.bak_mode)

    def on_cbox_mods_changed(self, widget, data=None):
        self.options.mods_mode = widget.props.active_id
        self.logger.debug("Mods mode changed: %s." % self.options.mods_mode)

    def on_cbox_modsonly_changed(self, widget, data=None):
        self.options.mods_only = widget.get_active()
        self.logger.debug("Modifiers only changed: {}.".format(
            self.options.mods_only))

    def on_cbox_visshift_changed(self, widget, data=None):
        self.options.vis_shift = widget.get_active()
        self.logger.debug("Visible Shift changed: {}.".format(
            self.options.vis_shift))

    def on_cbox_visspace_changed(self, widget, data=None):
        self.options.vis_space = widget.get_active()
        self.logger.debug("Show Whitespace changed: {}.".format(
            self.options.vis_space))

    def on_cbox_position_changed(self, widget, data=None):
        new_position = widget.props.active_id
        if self.options.position != 'fixed' and new_position == 'fixed':
            new_geom = self.on_btn_sel_geom(widget)
            if not new_geom:
                self.cbox_positions.props.active_id = self.options.position
                return
        self.options.position = new_position
        self.logger.debug("Window position changed: {}.".format(
            self.options.position))

    def on_cbox_screen_changed(self, widget, data=None):
        self.options.screen = widget.get_active()
        self.logger.debug("Screen changed: %d." % self.options.screen)

    def on_cbox_persist_changed(self, widget, data=None):
        self.options.persist = widget.get_active()
        if not self.get_property('visible'):
            self.show()
        else:
            self.on_label_change(self.label.get_text(), True)
        self.logger.debug("Persistent changed: %s." % self.options.persist)

    def on_sb_compr_changed(self, widget, data=None):
        self.options.compr_cnt = widget.get_value_as_int()
        self.logger.debug("Compress repeats value changed: {}.".format(
            self.options.compr_cnt))

    def on_cbox_compr_changed(self, widget, data=None):
        compr_enabled = widget.get_active()
        self.sb_compr.set_sensitive(compr_enabled)
        if compr_enabled:
            self.options.compr_cnt = self.sb_compr.get_value_as_int()
        else:
            self.options.compr_cnt = 0
        self.logger.debug("Compress repeats value changed: {}.".format(
            self.options.compr_cnt))

    def on_btn_sel_geom(self, widget, data=None):
        try:
            ret = subprocess.check_output(['slop', '-f', '%x %y %w %h %i'])
        except subprocess.CalledProcessError:
            return False
        except OSError:
            msg_txt = _("\"slop\" is required for interactive selection. "
                        "See <a href=\"{url}\">{url}</a>").format(
                            url=SLOP_URL)
            msg = Gtk.MessageDialog(parent=self,
                                    type=Gtk.MessageType.ERROR,
                                    buttons=Gtk.ButtonsType.OK,
                                    message_format="Error running \"slop\"")
            msg.format_secondary_markup(msg_txt)
            msg.run()
            msg.destroy()
            return False

        data = [int(x.decode('utf-8')) for x in ret.split()]
        self.options.geometry = data[0:4]
        self.options.window = data[4]
        xid = self.get_screen().get_root_window().get_xid()
        if not self.options.window or self.options.window == xid:
            # region selected, switch to fixed
            self.options.window = None
            self.options.position = 'fixed'
            self.cbox_positions.props.active_id = self.options.position

        self.btn_reset_geom.set_sensitive(True)
        return True

    def on_btn_reset_geom(self, widget, data=None):
        self.options.geometry = None
        if self.options.position == 'fixed':
            self.options.position = 'bottom'
            self.cbox_positions.props.active_id = self.options.position
        widget.set_sensitive(False)

    def on_adj_opacity_changed(self, widget, data=None):
        self.options.opacity = widget.get_value()

    def on_font_color_changed(self, widget, data=None):
        self.options.font_color = widget.get_color().to_string()

    def on_bg_color_changed(self, widget, data=None):
        self.options.bg_color = widget.get_color().to_string()

    def on_btn_font(self, widget, data=None):
        widget.props.label = widget.props.font
        self.options.font_desc = widget.props.font

    def on_toggle_keybinding(self, widget, *event, **user_data):
        widget.set_sensitive(False)
        waiting_keybinding = WaitKeybind()
        response = waiting_keybinding.run()
        if response == Gtk.ResponseType.ACCEPT:
            key_combination = waiting_keybinding.key_combination
            if key_combination == widget.get_text():
                self.show_alert(
                        _('This keybind is alredy assigned to another action'))
            else:
                widget.set_text(key_combination)
        else:
            self.show_invalid_alert()
        waiting_keybinding.destroy()
        widget.set_sensitive(True)

    def show_alert(self, primary_message, secondary_message=False):
        dialog = Gtk.MessageDialog(
            self,
            0,
            Gtk.MessageType.ERROR,
            Gtk.ButtonsType.OK,
            _(primary_message),
        )
        if secondary_message:
            dialog.format_secondary_text(
                _(secondary_message)
            )
        dialog.run()
        dialog.destroy()

    def show_invalid_alert(self):
        self.show_alert(
                _("Keybind error"),
                _("Shift and letters is not supported yet."))

    def load_keybinding(self):
        shortcut = None
        desktop_environment = get_desktop_environment()
        if desktop_environment in ['gnome', 'unity', 'cinnamon', 'mate']:
            if desktop_environment == 'gnome' or\
                    desktop_environment == 'unity':
                key = 'org.gnome.settings-daemon.plugins.media-keys'
                dcm = DConfManager(key)
                shortcuts = dcm.get_value('custom-keybindings')
                shortcuts = [shortcut.split('/')[-2] for shortcut in shortcuts]
                if 'screenkey' in shortcuts:
                    print(shortcuts)
                else:
                    self.save_keybinding()
                return ''
                key = 'org.gnome.settings-daemon.plugins.media-keys.\
custom-keybindings.screenkey'
            elif desktop_environment == 'cinnamon':
                key = 'org.cinnamon.desktop.keybindings.\
custom-keybindings.screenkey'
            elif desktop_environment == 'mate':
                key = 'org.mate.desktop.keybindings.screenkey'
            dcm = DConfManager(key)
            shortcut = dcm.get_value('binding')
        return shortcut

    def save_keybinding(self, key=None):
        desktop_environment = get_desktop_environment()
        if desktop_environment in ['gnome', 'unity', 'cinnamon', 'mate']:
            if desktop_environment == 'gnome' or\
                    desktop_environment == 'unity':
                key = 'org.gnome.settings-daemon.plugins.media-keys.\
custom-keybindings.screenkey'
            elif desktop_environment == 'cinnamon':
                key = 'org.cinnamon.desktop.keybindings.\
custom-keybindings.screenkey'
            elif desktop_environment == 'mate':
                key = 'org.mate.desktop.keybindings.screenkey'
            dcm = DConfManager(key)
            dcm.set_value('name', 'Screenkey')
            dcm.set_value('command', '/usr/bin/python3 \
/usr/share/screenkey/change_state.py')
            if key is None:
                key = ''
            dcm.set_value('binding', key)

    def init_ui(self):
        BaseDialog.init_ui(self)

        self.options = self.load_state()

        label = Gtk.Label.new('<b>{}</b>'.format(_('Time')))
        label.set_use_markup(True)
        frm_time = Gtk.Frame(label_widget=label,
                             border_width=4,
                             shadow_type=Gtk.ShadowType.NONE,
                             margin=6, hexpand=True)
        vbox_time = Gtk.Grid(orientation=VERTICAL,
                             row_spacing=6, margin=6)
        hbox_time = Gtk.Grid(column_spacing=6)
        lbl_time1 = Gtk.Label.new(_("Display for"))
        lbl_time2 = Gtk.Label.new(_("seconds"))
        sb_time = Gtk.SpinButton.new_with_range(0, 300, 1)
        sb_time.set_numeric(True)
        sb_time.set_digits(1)
        sb_time.set_update_policy(IF_VALID)
        sb_time.set_value(float(self.options.timeout))
        sb_time.connect("value-changed", self.on_sb_time_changed)
        hbox_time.add(lbl_time1)
        hbox_time.add(sb_time)
        hbox_time.add(lbl_time2)
        vbox_time.add(hbox_time)

        chk_persist = Gtk.CheckButton.new_with_label(_("Persistent window"))
        chk_persist.set_active(self.options.persist)
        chk_persist.connect("toggled", self.on_cbox_persist_changed)
        vbox_time.add(chk_persist)

        frm_time.add(vbox_time)

        label = Gtk.Label.new("<b>%s</b>" % _("Position"))
        label.set_use_markup(True)
        frm_position = Gtk.Frame(label_widget=label,
                                 border_width=4,
                                 shadow_type=Gtk.ShadowType.NONE,
                                 margin=6, hexpand=True)
        grid_position = Gtk.Grid(row_spacing=6, column_spacing=6,
                                 margin=6)

        lbl_screen = Gtk.Label.new(_("Screen"))
        lbl_screen.set_xalign(0)
        cbox_screen = Gtk.ComboBoxText()

        display = Gdk.Display.get_default()
        for i in range(display.get_n_monitors()):
            cbox_screen.insert_text(i, '{}: {}'.format(
                i, display.get_monitor(i).get_model()))
        cbox_screen.set_active(self.options.screen)
        cbox_screen.connect("changed", self.on_cbox_screen_changed)

        lbl_positions = Gtk.Label.new(_("Position"))
        lbl_positions.set_xalign(0)
        self.cbox_positions = Gtk.ComboBoxText(name='position')
        for id_, text in POSITIONS.items():
            self.cbox_positions.append(id_, text)
            if id_ == self.options.position:
                self.cbox_positions.props.active_id = id_
        self.cbox_positions.connect("changed", self.on_cbox_position_changed)

        self.btn_reset_geom = Gtk.Button.new_with_label(_("Reset"))
        self.btn_reset_geom.connect("clicked", self.on_btn_reset_geom)
        self.btn_reset_geom.set_sensitive(self.options.geometry is not None)

        hbox_position = Gtk.Grid(column_spacing=6, halign=END)
        hbox_position.add(self.cbox_positions)
        hbox_position.add(self.btn_reset_geom)

        btn_sel_geom = Gtk.Button.new_with_label(_("Select window/region"))
        btn_sel_geom.connect("clicked", self.on_btn_sel_geom)

        grid_position.add(lbl_screen)
        grid_position.attach_next_to(cbox_screen, lbl_screen, RIGHT, 1, 1)
        grid_position.attach_next_to(lbl_positions, lbl_screen, BOTTOM, 1, 1)
        grid_position.attach_next_to(hbox_position, lbl_positions, RIGHT, 1, 1)
        grid_position.attach_next_to(btn_sel_geom, lbl_positions, BOTTOM, 2, 1)

        label = Gtk.Label.new('<b>{}</b>'.format(_('Font')))
        label.set_use_markup(True)
        frm_aspect = Gtk.Frame(label_widget=label,
                               border_width=4,
                               shadow_type=Gtk.ShadowType.NONE,
                               margin=6, hexpand=True)
        grid_aspect = Gtk.Grid(row_spacing=6, column_spacing=6,
                               margin=6)

        frm_position.add(grid_position)

        lbl_font = Gtk.Label.new(_("Font"))
        lbl_font.set_xalign(0)
        btn_font = Gtk.FontButton.new_with_font(self.options.font_desc)
        btn_font.set_show_size(False)
        btn_font.set_show_style(True)
        btn_font.set_use_font(True)
        btn_font.connect("font-set", self.on_btn_font)

        lbl_sizes = Gtk.Label.new(_("Size"))
        lbl_sizes.set_xalign(0)
        cbox_sizes = Gtk.ComboBoxText(name='size')
        for id_, text in FONT_SIZES.items():
            cbox_sizes.append(id_, text)
            if id_ == self.options.font_size:
                cbox_sizes.props.active_id = id_
        cbox_sizes.connect("changed", self.on_cbox_sizes_changed)

        grid_aspect.add(lbl_font)
        grid_aspect.attach_next_to(btn_font, lbl_font, RIGHT, 1, 1)
        grid_aspect.attach_next_to(lbl_sizes, lbl_font, BOTTOM, 1, 1)
        grid_aspect.attach_next_to(cbox_sizes, lbl_sizes, RIGHT, 1, 1)
        frm_aspect.add(grid_aspect)

        label = Gtk.Label.new("<b>%s</b>" % _("Keybinding"))
        label.set_use_markup(True)
        """
        desktop_environment = get_desktop_environment()
        if desktop_environment in ['gnome', 'unity', 'cinnamon', 'mate']:
            frm_keybinding = Gtk.Frame(label_widget=label,
                                    border_width=4,
                                    shadow_type=Gtk.ShadowType.NONE,
                                    margin=6, hexpand=True)
            grid_keybinding = Gtk.Grid(row_spacing=6, column_spacing=6,
                                    margin=6)

            label_screenkey = Gtk.Label.new(_('Toggle Screenkey'))
            label_screenkey.set_halign(Gtk.Align.START)
            grid_keybinding.add(label_screenkey)

            keybinding = Gtk.Entry.new()
            keybinding.set_editable(False)
            keybinding.set_can_focus(False)
            keybinding.set_halign(Gtk.Align.CENTER)
            keybinding.set_text(self.load_keybinding())
            keybinding.connect('button-press-event',
                            self.on_toggle_keybinding)
            grid_keybinding.attach_next_to(keybinding, label_screenkey, RIGHT,
                                           1, 1)
            frm_keybinding.add(grid_keybinding)
        """

        label = Gtk.Label.new("<b>%s</b>" % _("Keys"))
        label.set_use_markup(True)
        frm_kbd = Gtk.Frame(label_widget=label,
                            border_width=4,
                            shadow_type=Gtk.ShadowType.NONE,
                            margin=6)
        grid_kbd = Gtk.Grid(row_spacing=6, column_spacing=6,
                            margin=6)

        lbl_modes = Gtk.Label.new(_("Keyboard mode"))
        lbl_modes.set_xalign(0)
        cbox_modes = Gtk.ComboBoxText(name='mode')
        for id_, text in KEY_MODES.items():
            cbox_modes.append(id_, text)
            if id_ == self.options.key_mode:
                cbox_modes.props.active_id = id_
        cbox_modes.connect("changed", self.on_cbox_modes_changed)

        lbl_bak = Gtk.Label.new(_("Backspace mode"))
        lbl_bak.set_xalign(0)
        cbox_bak = Gtk.ComboBoxText()
        for id_, text in BAK_MODES.items():
            cbox_bak.append(id_, text)
            if id_ == self.options.bak_mode:
                cbox_bak.props.active_id = id_
        cbox_bak.connect("changed", self.on_cbox_bak_changed)

        lbl_mods = Gtk.Label.new(_("Modifiers mode"))
        lbl_mods.set_xalign(0)
        cbox_mods = Gtk.ComboBoxText()
        for id_, text in MODS_MODES.items():
            cbox_mods.append(id_, text)
            if id_ == self.options.mods_mode:
                cbox_mods.props.active_id = id_
        cbox_mods.connect("changed", self.on_cbox_mods_changed)

        chk_modsonly = Gtk.CheckButton.new_with_label(
            _("Show Modifier sequences only"))
        chk_modsonly.set_active(self.options.mods_only)
        chk_modsonly.connect("toggled", self.on_cbox_modsonly_changed)

        chk_visshift = Gtk.CheckButton.new_with_label(_("Always show Shift"))
        chk_visshift.set_active(self.options.vis_shift)
        chk_visshift.connect("toggled", self.on_cbox_visshift_changed)

        chk_visspace = Gtk.CheckButton.new_with_label(
            _("Show Whitespace characters"))
        chk_visspace.set_active(self.options.vis_space)
        chk_visspace.connect("toggled", self.on_cbox_visspace_changed)

        hbox_compr = Gtk.Grid(column_spacing=6)
        chk_compr = Gtk.CheckButton.new_with_label(_("Compress repeats after"))
        chk_compr.set_active(self.options.compr_cnt > 0)
        chk_compr.connect("toggled", self.on_cbox_compr_changed)
        self.sb_compr = Gtk.SpinButton(digits=0,
                                       numeric=True,
                                       update_policy=IF_VALID,
                                       value=self.options.compr_cnt or 3)
        self.sb_compr.set_increments(1, 1)
        self.sb_compr.set_range(1, 100)
        self.sb_compr.connect("value-changed", self.on_sb_compr_changed)
        hbox_compr.add(chk_compr)
        hbox_compr.add(self.sb_compr)

        grid_kbd.add(lbl_modes)
        grid_kbd.attach_next_to(cbox_modes, lbl_modes, RIGHT, 1, 1)
        grid_kbd.attach_next_to(lbl_bak, lbl_modes, BOTTOM, 1, 1)
        grid_kbd.attach_next_to(cbox_bak, lbl_bak, RIGHT, 1, 1)
        grid_kbd.attach_next_to(lbl_mods, lbl_bak, BOTTOM, 1, 1)
        grid_kbd.attach_next_to(cbox_mods, lbl_mods, RIGHT, 1, 1)
        grid_kbd.attach_next_to(chk_modsonly, lbl_mods, BOTTOM, 2, 1)
        grid_kbd.attach_next_to(chk_visshift, chk_modsonly, BOTTOM, 2, 1)
        grid_kbd.attach_next_to(chk_visspace, chk_visshift, BOTTOM, 2, 1)
        grid_kbd.attach_next_to(hbox_compr, chk_visspace, BOTTOM, 2, 1)
        frm_kbd.add(grid_kbd)

        label = Gtk.Label.new("<b>%s</b>" % _("Color"))
        label.set_use_markup(True)
        frm_color = Gtk.Frame(label_widget=label,
                              border_width=4,
                              shadow_type=Gtk.ShadowType.NONE,
                              margin=6)
        grid_color = Gtk.Grid(orientation=VERTICAL,
                              row_spacing=6, column_spacing=6,
                              margin=6)

        lbl_font_color = Gtk.Label.new(_("Font color"))
        lbl_font_color.set_xalign(0)
        btn_font_color = Gtk.ColorButton(color=Gdk.color_parse(
            self.options.font_color),
                                         title=_("Text color"),
                                         halign=END)
        btn_font_color.connect("color-set", self.on_font_color_changed)

        lbl_bg_color = Gtk.Label.new(_("Background color"))
        lbl_bg_color.set_xalign(0)
        btn_bg_color = Gtk.ColorButton(color=Gdk.color_parse(
            self.options.bg_color),
                                       title=_("Background color"),
                                       halign=END)
        btn_bg_color.connect("color-set", self.on_bg_color_changed)

        lbl_opacity = Gtk.Label.new(_("Opacity"))
        lbl_opacity.set_xalign(0)
        adj_opacity = Gtk.Adjustment.new(self.options.opacity,
                                         0, 1.0, 0.1, 0, 0)
        adj_opacity.connect("value-changed", self.on_adj_opacity_changed)
        adj_scale = Gtk.Scale(adjustment=adj_opacity,
                              hexpand=True, halign=FILL)

        grid_color.add(lbl_font_color)
        grid_color.attach_next_to(btn_font_color, lbl_font_color, RIGHT, 1, 1)
        grid_color.attach_next_to(lbl_bg_color, lbl_font_color, BOTTOM, 1, 1)
        grid_color.attach_next_to(btn_bg_color, lbl_bg_color, RIGHT, 1, 1)
        grid_color.attach_next_to(lbl_opacity, lbl_bg_color, BOTTOM, 1, 1)
        grid_color.attach_next_to(adj_scale, lbl_opacity, RIGHT, 1, 1)
        frm_color.add(grid_color)

        vbox_main = Gtk.Grid(orientation=VERTICAL)
        vbox_main.add(frm_time)
        vbox_main.add(frm_position)
        vbox_main.add(frm_aspect)
        """
        if desktop_environment in ['gnome', 'unity', 'cinnamon', 'mate']:
            vbox_main.add(frm_keybinding)
        """
        self.grid.add(vbox_main)
        vbox_main = Gtk.Grid(orientation=VERTICAL)
        vbox_main.add(frm_kbd)
        vbox_main.add(frm_color)
        self.grid.add(vbox_main)


if __name__ == '__main__':
    import logging
    logging.basicConfig(level=logging.DEBUG)
    logger = logging.getLogger('ScreenKey')
    preferencesDialog = PreferencesDialog(logger)
    preferencesDialog.run()
